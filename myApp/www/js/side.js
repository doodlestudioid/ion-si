angular.module('ionicApp', ['ionic'])

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('zakatmenu', {
      url: "/zakat",
      abstract: true,
      templateUrl: "templates/zakat-menu.html"
    })
    .state('zakatmenu.home', {
      url: "/home",
      views: {
        'menuContent' :{
          templateUrl: "templates/home.html"
        }
      }
    })
    .state('zakatmenu.campaign', {
      url: "/campaign",
      views: {
        'menuContent' :{
          templateUrl: "templates/campaign.html"
        }
      }
    })
    .state('zakatmenu.sedekah', {
      url: "/sedekah",
      views: {
        'menuContent' :{
          templateUrl: "templates/sedekah.html"
        }
      }
    })
    .state('zakatmenu.login', {
      url: "/login",
      views: {
        'menuContent' :{
          templateUrl: "templates/login.html"
        }
      }
    })
    .state('zakatmenu.signup', {
      url: "/signup",
      views: {
        'menuContent' :{
          templateUrl: "templates/signup.html"
        }
      }
    })
  $urlRouterProvider.otherwise("/zakat/home");
})

.controller('MainCtrl', function($scope, $ionicSideMenuDelegate) {
  $scope.attendees = [
  ];
  
  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };
})